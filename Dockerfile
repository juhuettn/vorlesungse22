FROM python:3.10.8-slim-buster

RUN pwd

RUN ls -l

RUN mkdir -p /src

WORKDIR /src

COPY main.py /src

CMD python main.py
